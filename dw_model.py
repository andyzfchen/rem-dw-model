import numpy as np
import matplotlib.pyplot as plt

class DWModel(object):
  ''' Model for Real Estate Market as described by Denise DiPasquale and William C. Wheaton '''

  def __init__(self):
    self.alpha  = 0   # effect of rent on demand
    self.beta   = 0   # effect of economic health on demand
    self.gamma  = 0   # cost of construction by construction level
    self.delta  = 0   # minimum price to start construction
    self.ir     = 0   # capitalization rate
    self.dr     = 0   # depreciation rate
    self.eh     = 0   # economic health

    self.n      = 0   # number of time evolutions
    self.ic     = 1   # starting initial condition
    self.R0     = 0   # initial rent
    self.P0     = 0   # initial price
    self.C0     = 0   # initial construction level
    self.S0     = 0   # initial supply

    self.Req    = 0   # equilibrium rent
    self.Peq    = 0   # equilibrium price
    self.Ceq    = 0   # equilibrium construction level
    self.Seq    = 0   # equilibrium supply

  def findEq(self):
    try:
      tempSeq    = (self.beta * self.eh - self.ir * self.alpha * self.delta) / (1 + self.alpha * self.gamma * self.dr * self.ir)
      tempReq    = (self.beta * self.eh - tempSeq) / self.alpha
      tempPeq    = tempReq / self.ir
      tempCeq    = (tempPeq - self.delta) / self.gamma

      if (abs(tempCeq / self.dr - tempSeq) < 1e-9):
        self.Req = tempReq
        self.Peq = tempPeq
        self.Ceq = tempCeq
        self.Seq = tempSeq

        #print("Req:   %f" % self.Req)
        #print("Peq:   %f" % self.Peq)
        #print("Ceq:   %f" % self.Ceq)
        #print("Seq:   %f" % self.Seq)

        return self.Req, self.Peq, self.Ceq, self.Seq
      else:
        print("Unable to set equilibrium state.")
    except:
      print("Unable to set equilibrium state.")

  def plotEq(self, hplot=plt):
    #print("Plotting Equilibrium Path")
    self.findEq()
    xEq = np.array([-self.Peq,self.Seq,-self.Peq])
    xEq = np.repeat(xEq,2)
    yEq = np.array([self.Req,-self.Ceq,self.Req])
    yEq = np.repeat(yEq,2)
    hplot.plot(xEq[:-1],yEq[1:],color="k",linestyle="--")

  def plotQuadCurves(self, hplot=plt):
    #print("Plotting Quadrant Curves")
    xaxis = np.linspace(-1,1,1000)
    hplot.plot(xaxis,np.zeros(1000),color="k")
    hplot.plot([0,0],[-1,1],color="k")
    hplot.set_xlim(-1,1)
    hplot.set_ylim(-1,1)

    xS = np.linspace(0,1,1000)
    yR = (self.beta * self.eh - xS) / self.alpha
    hplot.plot(xS,yR)

    xR = np.linspace(0,1,1000)
    yP = xR / self.ir
    hplot.plot(-yP,xR)

    xP = np.linspace(0,1,1000)
    yC = (xP - self.delta) / self.gamma
    hplot.plot(-xP,-yC)

    xC = np.linspace(0,1,1000)
    yS = xC / self.dr
    hplot.plot(yS,-xC)

  def timeEvol(self, n=5, ic=1):
    n += ic
    Rarray = np.zeros(n//4+2)
    Parray = np.zeros(n//4+2)
    Carray = np.zeros(n//4+2)
    Sarray = np.zeros(n//4+2)

    ics = [self.S0, self.R0, self.P0, self.C0]
    arrays = [Sarray, Rarray, Parray, Carray]

    arrays[ic][0] = ics[ic]

    for i in range(ic+1,n+1):
      #print(i)
      if ((i%4) == 0):
        Sarray[i//4] = Carray[i//4-1] / self.dr
      elif ((i%4) == 1):
        Rarray[i//4] = (self.beta * self.eh - Sarray[i//4]) / self.alpha
      elif ((i%4) == 2):
        Parray[i//4] = Rarray[i//4] / self.ir
      elif ((i%4) == 3):
        Carray[i//4] = (Parray[i//4] - self.delta) / self.gamma

    return Rarray, Parray, Carray, Sarray

  def plotTimeEvol(self, n=5, ic=1, hplot=plt):
    #print("Plotting Time Evolution")
    R, P, C, S = self.timeEvol(n=n, ic=ic)
    #print(S)
    #print(R)
    #print(P)
    #print(C)

    arrays = [S, R, -P, -C]
    x = np.ravel([-P,S],order='F')
    x = np.repeat(x,2)
    y = np.ravel([R,-C],order='F')
    y = np.repeat(y,2)
    z = np.ravel([S, R, -P, -C], order='F')
    #print(x)
    #print(y)
    #print(z)

    coord = np.zeros((2,n+ic))

    for i in range(ic,n+ic):
      coord[(i%2),i] = z[i]
      coord[((i+1)%2),i] = z[i+1]
    #print(coord)

    # time evolution path
    hplot.plot(coord[0,ic:],coord[1,ic:],linestyle='-',linewidth=0.1,color='m')
    # starting point
    hplot.plot(coord[0,ic],coord[1,ic],marker="o",markersize=4,color='m')
    # ending point
    hplot.plot(coord[0,-1],coord[1,-1],marker="x",markersize=4,color='m')










