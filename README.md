# Real Estate Model

This linear model widget is based on the market model described by Denise DiPasquale and William C. Wheaton.

## Usage ##

All executables to run the widget are located in

```
./dist/
```

Windows executable: `dwWindow.exe`

Mac OSX app: `dwWindow.app`

## Utilities ##

1. Calculate equilibrium path.
2. Calculate time evolution path given initial conditions.
3. Help window with model equations and parameter definitions.
