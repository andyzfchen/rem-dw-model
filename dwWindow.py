from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QSizePolicy
import sys
import os
import subprocess
import dwGUI
import dwHelp
#import matplotlib
#matplotlib.use('Qt5Agg')
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import dw_model as DW

class PlotCanvas(FigureCanvasQTAgg):
  def __init__(self, parent=None, width=5, height=4, dpi=100):
    fig = Figure(figsize=(width, height), dpi=dpi)
    self.axes = fig.add_subplot(111)
    super(PlotCanvas, self).__init__(fig)

    FigureCanvasQTAgg.__init__(self, fig)
    self.setParent(parent)

    FigureCanvasQTAgg.setSizePolicy(self, QSizePolicy.Expanding, QSizePolicy.Expanding)
    FigureCanvasQTAgg.updateGeometry(self)
    plt.rcParams["font.size"]=4
    plt.rcParams["lines.linewidth"]=0.8
    plt.rcParams["axes.linewidth"]=0.5
    plt.rcParams["figure.frameon"]=False
    self.axes.spines['top'].set_visible(False)
    self.axes.spines['bottom'].set_visible(False)
    self.axes.spines['left'].set_visible(False)
    self.axes.spines['right'].set_visible(False)
    self.axes.xaxis.set_visible(False)
    self.axes.yaxis.set_visible(False)
    fig.tight_layout()

class HelpWindow(QtWidgets.QMainWindow, dwHelp.Ui_MainWindow):
  def __init__(self, parent=None):
    super(HelpWindow, self).__init__(parent)
    self.setupUi(self)

    ########################
    ########################

    # quit button
    self.pushButton.clicked.connect(self.close)
    
    ########################
    ########################

class ExampleApp(QtWidgets.QMainWindow, dwGUI.Ui_MainWindow):
  def __init__(self, parent=None):
    super(ExampleApp, self).__init__(parent)
    self.setupUi(self)

    ########################
    ########################

    # quit button
    self.pushButton_3.clicked.connect(self.exit)
    self.pushButton_2.clicked.connect(self.help)

    ########################
    # Setup
    ########################

    self.dw = DW.DWModel()

    ########################
    # Parameters
    ########################

    self.pushButton.clicked.connect(self.updatePlot)
    self.pushButton_4.clicked.connect(self.resetParams)

    self.horizontalSlider.sliderMoved.connect(self.updatePlot)
    self.horizontalSlider_2.sliderMoved.connect(self.updatePlot)
    self.horizontalSlider_3.sliderMoved.connect(self.updatePlot)
    self.horizontalSlider_4.sliderMoved.connect(self.updatePlot)
    self.horizontalSlider_5.sliderMoved.connect(self.updatePlot)
    self.horizontalSlider_6.sliderMoved.connect(self.updatePlot)
    self.horizontalSlider_7.sliderMoved.connect(self.updatePlot)
    self.horizontalSlider_8.sliderMoved.connect(self.updatePlot)
    self.horizontalSlider_9.sliderMoved.connect(self.updatePlot)
    self.horizontalSlider_10.sliderMoved.connect(self.updatePlot)
    self.horizontalSlider_11.sliderMoved.connect(self.updatePlot)
    self.horizontalSlider_12.sliderMoved.connect(self.updatePlot)

    ########################
    # Time Evolution
    ########################

    self.radioButton.toggled.connect(self.updatePlot)
    self.radioButton_2.toggled.connect(self.updatePlot)
    self.radioButton_3.toggled.connect(self.updatePlot)
    self.radioButton_4.toggled.connect(self.updatePlot)

    self.checkBox.stateChanged.connect(self.updatePlot)

    ########################
    # Plotting
    ########################

    self.m = PlotCanvas(self, width=5, height=4, dpi=100)
    self.m.move(400,70)
    self.m.setGeometry(QtCore.QRect(430, 100, 451, 451))
    self.updatePlot()

    ########################
    ########################

  def exit(self):
    exit()

  def help(self):
    self.dialog = HelpWindow(self)
    self.dialog.show()

  def updateParams(self):
    self.plainTextEdit.setPlainText("%.3f" % (float(self.horizontalSlider.value())/1000.))
    self.plainTextEdit_2.setPlainText("%.3f" % (float(self.horizontalSlider_2.value())/1000.))
    self.plainTextEdit_3.setPlainText("%.3f" % (float(self.horizontalSlider_3.value())/1000.))
    self.plainTextEdit_4.setPlainText("%.3f" % (float(self.horizontalSlider_4.value())/1000.))
    self.plainTextEdit_5.setPlainText("%.3f" % (float(self.horizontalSlider_5.value())/1000.))
    self.plainTextEdit_6.setPlainText("%.3f" % (float(self.horizontalSlider_6.value())/1000.))
    self.plainTextEdit_7.setPlainText("%.3f" % (float(self.horizontalSlider_7.value())/1000.))

    self.dw.alpha = float(self.plainTextEdit.toPlainText())
    self.dw.beta  = float(self.plainTextEdit_2.toPlainText())
    self.dw.gamma = float(self.plainTextEdit_3.toPlainText())
    self.dw.delta = float(self.plainTextEdit_4.toPlainText())
    self.dw.ir    = float(self.plainTextEdit_5.toPlainText())
    self.dw.dr    = float(self.plainTextEdit_6.toPlainText())
    self.dw.eh    = float(self.plainTextEdit_7.toPlainText())

  def updateIC(self):
    self.plainTextEdit_8.setPlainText("%d" % (float(self.horizontalSlider_8.value())))
    self.plainTextEdit_9.setPlainText("%.3f" % (float(self.horizontalSlider_9.value())/1000.))
    self.plainTextEdit_10.setPlainText("%.3f" % (float(self.horizontalSlider_10.value())/1000.))
    self.plainTextEdit_11.setPlainText("%.3f" % (float(self.horizontalSlider_11.value())/1000.))
    self.plainTextEdit_12.setPlainText("%.3f" % (float(self.horizontalSlider_12.value())/1000.))

    self.dw.n   = int(self.plainTextEdit_8.toPlainText())
    self.dw.R0  = float(self.plainTextEdit_9.toPlainText())
    self.dw.P0  = float(self.plainTextEdit_10.toPlainText())
    self.dw.C0  = float(self.plainTextEdit_11.toPlainText())
    self.dw.S0  = float(self.plainTextEdit_12.toPlainText())

    if self.radioButton.isChecked():
      self.dw.ic = 1
    elif self.radioButton_2.isChecked():
      self.dw.ic = 2
    elif self.radioButton_3.isChecked():
      self.dw.ic = 3
    elif self.radioButton_4.isChecked():
      self.dw.ic = 0

  def updateEq(self):
    Req, Peq, Ceq, Seq = self.dw.findEq()

    self.plainTextEdit_13.setPlainText(str(Req)[:5])
    self.plainTextEdit_14.setPlainText(str(Peq)[:5])
    self.plainTextEdit_15.setPlainText(str(Ceq)[:5])
    self.plainTextEdit_16.setPlainText(str(Seq)[:5])

    self.plainTextEdit.repaint()

  def updatePlot(self):
    self.m.axes.cla()

    self.updateParams()
    self.updateEq()

    self.dw.plotQuadCurves(self.m.axes)
    self.dw.plotEq(self.m.axes)

    if self.checkBox.isChecked():
      self.updateIC()
      self.dw.plotTimeEvol(n=self.dw.n, ic=self.dw.ic, hplot=self.m.axes)

    self.m.draw()
    self.m.repaint()

  def resetParams(self):
    # reset parameters
    self.plainTextEdit.setPlainText("1")
    self.plainTextEdit_2.setPlainText("1")
    self.plainTextEdit_3.setPlainText("1")
    self.plainTextEdit_4.setPlainText("0")
    self.plainTextEdit_5.setPlainText("1")
    self.plainTextEdit_6.setPlainText("1")
    self.plainTextEdit_7.setPlainText("1")

    self.horizontalSlider.setSliderPosition(1000)
    self.horizontalSlider_2.setSliderPosition(1000)
    self.horizontalSlider_3.setSliderPosition(1000)
    self.horizontalSlider_4.setSliderPosition(0)
    self.horizontalSlider_5.setSliderPosition(1000)
    self.horizontalSlider_6.setSliderPosition(1000)
    self.horizontalSlider_7.setSliderPosition(1000)

    # reset initial conditions
    self.plainTextEdit_8.setPlainText("5")
    self.plainTextEdit_9.setPlainText("0.5")
    self.plainTextEdit_10.setPlainText("0.5")
    self.plainTextEdit_11.setPlainText("0.5")
    self.plainTextEdit_12.setPlainText("0.5")

    self.horizontalSlider_8.setSliderPosition(5)
    self.horizontalSlider_9.setSliderPosition(500)
    self.horizontalSlider_10.setSliderPosition(500)
    self.horizontalSlider_11.setSliderPosition(500)
    self.horizontalSlider_12.setSliderPosition(500)

    self.updatePlot()
    

def main():
  app = QApplication(sys.argv)
  form = ExampleApp()
  form.show()
  app.exec_()

if __name__ == '__main__':
  main()
