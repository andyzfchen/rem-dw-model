# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dwHelp.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(640, 583)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(10, 10, 241, 16))
        self.label.setObjectName("label")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(562, 10, 71, 32))
        self.pushButton.setObjectName("pushButton")
        self.plainTextEdit = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.plainTextEdit.setGeometry(QtCore.QRect(10, 50, 621, 481))
        font = QtGui.QFont()
        font.setFamily("Andale Mono")
        self.plainTextEdit.setFont(font)
        self.plainTextEdit.setReadOnly(True)
        self.plainTextEdit.setTabStopWidth(40)
        self.plainTextEdit.setObjectName("plainTextEdit")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 640, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Real Estate Market Model Help Box"))
        self.pushButton.setText(_translate("MainWindow", "Close"))
        self.plainTextEdit.setPlainText(_translate("MainWindow", "This real estate model is a linear approximation model used to simulate the interaction among rent price, building valuation, level of building construction, and supply of real estate.\n"
"\n"
"DW Model:                Linear Model:\n"
"D(R,E) = S               D(R,E) = -α R + β E\n"
"P = R / i                P = R / i\n"
"P = CC = f(C)            f(C) = γ C + δ\n"
"S = C / d                S = C / d\n"
"\n"
"Variable and parameter definitions:\n"
"R:     Rent Price\n"
"P:     Value of Real Estate\n"
"C:     Construction Level\n"
"S:     Supply of Real Estate\n"
"\n"
"D:     Demand of Real Estate\n"
"E:     Health of Economy\n"
"CC:    Construction Cost\n"
"i:     Capitalization Rate\n"
"d:     Depreciaction Rate\n"
"\n"
"α:     Effect of Rent on Demand\n"
"β:     Effect of Economy on Demand\n"
"γ:     Cost of Construction per Construction Level\n"
"δ:     Minimum Price to Begin Construction\n"
"\n"
"Graph Details:\n"
"The curves and paths are plotted in x-y Cartesian coordinates that extend one unit interval in positive and negative direction. Parameters are assumed to contain units such that the model can be scaled to \"unit\" quadrants."))
