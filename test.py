import dw_model as DW
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.figure import Figure

test = DW.DWModel()

test.alpha  = 1
test.beta   = 1
test.gamma  = 1
test.delta  = 0
test.ir     = 1
test.dr     = 1
test.eh     = 1
test.ur     = 1

test.n      = 5
test.ic     = 1
test.R0     = 0.5
test.P0     = 0.5
test.C0     = 0.5
test.S0     = 0.5

#fig = Figure(figsize=(5, 4), dpi=100)
fig = plt.figure()
axes = fig.add_subplot(111)

# plotting quadrant curves
test.plotQuadCurves(hplot=axes)

# equilibrium path
test.plotEq(hplot=axes)

# time evolution
test.plotTimeEvol(n=test.n, ic=test.ic, hplot=axes)

fig.show()
#fig.savefig("asdf.pdf")







